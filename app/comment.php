<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['berita_id', 'user_id', 'komentar'];
    
    public function berita()
    {
        return $this->belongsTo(Berita::class);
    }

    public function users()
    {
        return $this->belongsTo(User::class);
    }
}
