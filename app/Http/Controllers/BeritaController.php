<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Berita;
use DB;

class BeritaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $berita = DB::table('berita')->get();
        return view('admin.berita.index', compact('berita'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $admins = User::all();
        return view('admin.berita.create', compact('admins'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'judul' => 'required',
            'konten' => 'required',
            'penulis' => 'required',
            'user_id' => 'required',
        ]);

        Berita::create([
            'judul' => $request->judul,
            'konten' => $request->konten,
            'penulis' => $request->penulis,
            'user_id' => $request->user_id
        ]);
        
        return redirect('/berita');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $berita = DB::table('berita')->where('id', $id)->first();
        return view('admin.berita.show', compact('berita'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = User::all();
        $berita = Berita::findorfail($id);
        return view('admin.berita.edit', compact('berita', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // $this->validate($request,[
        //     'judul' => 'required',
        //     'konten' => 'required',
        //     'penulis' => 'required',
        //     'user_id' => 'required',
        // ]);
        $berita_data =  [
            'judul' => $request->judul,
            'konten' => $request->konten,
            'penulis' => $request->penulis,
            'user_id' => $request->user_id
        ];
        berita::whereId($id)->update($berita_data);
        return redirect('/berita');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $berita = Berita::findorfail($id);
        $berita->delete();
        return redirect('/berita');
    }
}
