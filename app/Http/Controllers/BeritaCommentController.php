<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Berita;
use App\Comment;

class BeritaCommentController extends Controller
{
    public function store(Request $request, Berita $berita)
    {
        $berita->comments()->create(array_merge(
                                    $request->only('komentar'),
                                    ['user_id' => auth()->id()]
                                ));
                                
        return redirect()->back();
    }
}
