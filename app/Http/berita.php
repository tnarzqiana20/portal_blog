<?php

namespace App\Providers;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $table ="berita";
    protected $fillable = ['judul', 'konten', 'penulis', 'user_id'];

    ///public function category()
    ///{
       /// return $this->belongsTo(Category::class);
    ///}

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
