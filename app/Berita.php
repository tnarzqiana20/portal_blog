<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $table = "berita";
    protected $fillable = ["judul", "konten", "penulis", "user_id"];
    
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }
}
