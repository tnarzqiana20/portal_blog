@extends('admin.layout.master')
@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.24/datatables.min.css"/>
@endpush
@section('title')
    User id : {{ Auth::user()->name }}
@endsection
@section('content')
{{-- User id : {{ Auth::user()->name }} --}}

@if ($profile != null)
<div class="container">
    <form action="">
        <div class="form-group">
            <label for="name">Nama Lengkap : {{Auth::user()->name}}</label><br>
            <label for="">Email : {{Auth::user()->email}}</label>
        </div>
    </form>
    <hr>
    <form method="POST" action="{{ route('profil.update', [$profile->id]) }}">
        @method('PUT')
        @csrf
        <div class="form-group">
            <label for="umur">Umur</label>
            <input type="number" id="umur" name="umur" value="{{ $profile->umur }}" class="form-control" placeholder="Umur" autofocus>
        </div>
        <div class="fomr-group">
            <label for="alamat">Alamat</label>
            <textarea name="alamat" value="" id="alamat" cols="30" rows="10" class="form-control">{{ $profile->alamat }}</textarea>
        </div>
        <div class="form-group">
            <div class="row mt-3">
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary btn-block">Edit</button>
                </div>
                <div class="col-md-6">
                    <button type="reset" class="btn btn-warning btn-block">Reset</button>
                </div>
            </div>
        </div>    
    </form>
</div>
@else
    {{-- insert profil --}}
<div class="container">
    <form action="{{ route('profil.store') }}" method="POST">
        @csrf
        <div class="form-group">
            <label for="umur">Umur</label>
            <input type="number" id="umur" name="umur" class="form-control" placeholder="Umur" autofocus>
        </div>
        <div class="fomr-group">
            <label for="alamat">Alamat</label>
            <textarea name="alamat" id="alamat" cols="30" rows="10" class="form-control"></textarea>
        </div>
        <div class="form-group">
            <div class="row mt-3">
                <div class="col-md-6">
                    <button type="submit" class="btn btn-primary btn-block">Simpan</button>
                </div>
                <div class="col-md-6">
                    <button type="reset" class="btn btn-warning btn-block">Reset</button>
                </div>
            </div>
        </div>    
    </form>
</div>
@endif



{{-- <table id="example1" class="table table-bordered table-striped">
    <thead>
        <tr>
        <th>Umur</th>
        <th>Alamat</th>
        <th>Platform(s)</th>
        <th>Engine version</th>
        <th>CSS grade</th>
        </tr>
    </thead>
    <tbody>
        <tr>
        <td>Trident</td>
        <td>Internet
            Explorer 4.0
        </td>
        <td>Win 95+</td>
        <td> 4</td>
        <td>X</td>
        </tr>
    </tbody>
    <tfoot>
        <tr>
            <th>Rendering engine</th>
            <th>Browser</th>
            <th>Platform(s)</th>
            <th>Engine version</th>
            <th>CSS grade</th>
        </tr>
    </tfoot>
    </table>
 --}}
@endsection
@push('js')
<script src="{{ asset('asset/plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('asset/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
<script>
  $(function () {
    $("#example1").DataTable();
  });
</script>
@endpush
