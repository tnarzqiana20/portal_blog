<header class="header">
    <div class="header__top">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="header__top__left">
                        {{-- <ul>
                            <li><i class="fa fa-envelope"></i> hello@colorlib.com</li>
                            <li>Free Shipping for all Order of $99</li>
                        </ul> --}}
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="header__top__right">
                        <div class="header__top__right__auth">

                            @if (Route::has('login'))
                                <div class="top-right links">
                                    @auth
                                        <div class="header__top__right__auth">
                                            <a href="{{ url('/home') }}">Home</a>
                                        </div>
                                        @else
                                        <div class="header__top__right__auth">
                                            <a href="{{ route('login') }}"><i class="fa fa-user"></i> Login</a>
                                        </div>
                                        <div class="header__top__right__auth">
                                            |
                                        </div>
                                        @if (Route::has('register'))
                                        <div class="header__top__right__auth">
                                            <a href="{{ route('register') }}"><i class="fa fa-user"></i> Register</a>
                                        </div>
                                        @endif
                                    @endauth
                                </div>
                            @endif
                        </div>
                            {{-- <a href="#"><i class="fa fa-user"></i> Login</a>
                        </div>
                        <div class="header__top__right__auth">
                            |
                        </div>
                        <div class="header__top__right__auth">
                            <a href="#"><i class="fa fa-user"></i> Register</a>
                        </div> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="header__logo">
                    <a href="./index.html"><img src="{{ asset('users/img/logo.png') }}" alt=""></a>
                </div>
            </div>
            <div class="col-lg-6">
                <nav class="header__menu">
                    <ul>
                        <li class="active"><a href="./index.html">Home</a></li>
                        <li><a href="./blog.html">Blog</a></li>
                        <li><a href="./contact.html">Contact</a></li>
                    </ul>
                </nav>
            </div>
            <hr>
        </div>
        <div class="humberger__open">
            <i class="fa fa-bars"></i>
        </div>
    </div>
</header>