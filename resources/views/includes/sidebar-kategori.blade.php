<section class="hero">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="hero__categories">
                    <div class="hero__categories__all">
                        <i class="fa fa-bars"></i>
                        <span>Kategori</span>
                    </div>
                    <ul>
                        <li><a href="#">Kesehatan</a></li>
                        <li><a href="#">Pendidikan</a></li>
                        <li><a href="#">Politik</a></li>
                        <li><a href="#">Selebrita</a></li>
                        <li><a href="#">Luar Negeri</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="hero__item set-bg" data-setbg="{{ asset('users/img/users/1.jpg') }}" style="background-image: url(&quot;img/hero/banner.jpg&quot;);">
                    <div class="hero__text">
                        {{-- <span>FRUIT FRESH</span>
                        <h2>Vegetable <br>100% Organic</h2>
                        <p>Free Pickup and Delivery Available</p>
                        <a href="#" class="primary-btn">SHOP NOW</a> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>