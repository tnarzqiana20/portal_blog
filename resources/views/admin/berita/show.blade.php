@extends('admin.layout.master')

  @section('title')
    Show berita id: {{$berita->id}}
  @endsection


  @section('content')
  {{-- @include('comment.php') --}}
  <p>{{$berita->judul}}</p>
  <p>{{$berita->konten}}</p>
  <p>{{$berita->penulis}}</p>

  <div class="panel panel-default">
      <div class="panel-heading">Tambahkan Komentar</div>
      @foreach ($berita->comment()->get() as $comment)
        <div class="panel-body">
            <h4>{{$comment->users->name}} - {{$comment->created_at->diffForHumans()}}</h4>

            <p>{{$comment->komentar}}</p>
        </div>
          
      @endforeach
      <div class="panel-body">
          <form action="{{route('berita.comment.store', $berita)}}" method="post" class="form-horizontal">
            {{csrf_field()}}
                <textarea name="komentar" id="" cols="30" rows="5" class="form-control" placeholder="Berikan Komentar"></textarea>
                           
                <input type="submit" value="Komentar" class ="btn btn-primary">
              
          </form>
        </div>
    </div> 

  @endsection

  