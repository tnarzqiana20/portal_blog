@extends('admin.layout.master')
@push('css')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.24/datatables.min.css"/>
@endpush

  @section('title')
    Daftar Berita
  @endsection


  @section('content')
    <a href="{{ route('berita.create') }}" class="btn btn-primary my-2">Tambah</a>

    <table class="table">
        <thead class="thead-light">
            <tr>
            <th scope="col">#</th>
            <th scope="col">Judul</th>
            {{-- <th scope="col">Konten</th> --}}
            <th scope="col">Penulis</th>
            <th scope="col">Actions</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($berita as $key=>$value)
                <tr>
                    <td>{{$key + 1}}</th>
                    <td>{{$value->judul}}</td>
                    {{-- <td>{{$value->konten}}</td> --}}
                    <td>{{$value->penulis}}</td>
                    <td>
                        <form action="/berita/{{$value->id}}" method="POST">
                            <a href="/berita/{{$value->id}}" class="btn btn-info">Show</a>
                            <a href="{{ route('berita.edit', [$value->id]) }}" class="btn btn-primary">Edit</a>
                            @csrf
                            @method('DELETE')
                            <input type="submit" class="btn btn-danger my-1" value="Delete">
                        </form>
                    </td>
                </tr>
            @empty
                <tr colspan="3">
                    <td>No data</td>
                </tr>  
            @endforelse              
        </tbody>
    </table>
  @endsection
  @push('js')
    {{-- <script src="http://cdn.datatables.net/1.10.24/js/jquery.dataTables.min.js"></script> --}}
    <script src="{{ asset('admin/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin/plugins/datatables-bs4/js/dataTables.bootstrap4.js') }}"></script>
    <script>
    $(function () {
        $("#example1").DataTable();
    });
    </script>
@endpush
