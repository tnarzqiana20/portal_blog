@extends('admin.layout.master')

  @section('title')
    Tambah Berita
  @endsection


  @section('content')
    <div>
        <form method="POST" action="{{ route('berita.store') }}">
            @csrf
            {{-- @method('POST') --}}
            <div class="form-group">
                <label for="title">Judul</label>
                <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan Judul Berita">
                @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Konten</label>
                <input type="text" class="form-control" name="konten" id="konten" placeholder="Masukkan Konten">
                @error('umur')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Penulis</label>
                <input type="text" class="form-control" name="penulis" id="penulis" placeholder="Masukkan Nama Penulis">
                @error('penulis')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
            </div>
            <div class="form-group">
                <label for="body">Email</label>
                <select name="user_id" id="email"  class="form-control">
                    <option value="">-</option>
                    @foreach ($admins as $value)
                        <option value="{{$value->id}}">{{$value->email}}</option>
                    @endforeach
                </select>
            @error('user_id')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror   
            </div>
            <button type="submit" class="btn btn-primary">Tambah</button>
        </form>
    </div>

  

  @endsection

  