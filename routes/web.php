<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.global');
});
// Route::get('/', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@user')->name('home/user');
Route::get('/home/admin', 'HomeController@adminHome')->name('home/admin')->middleware('level');
Route::resource('profil', 'ProfilController');
Route::resource('user', 'UsersController');

/*KETERANGAN CONTOH
Contoh Untuk Tampilan user : user.layout.master
Contoh Untuk Tampilan admin : admin.layout.master
*/
// Route::get('/master',function(){
//     return view('admin.layout.master');
// });

// Route::get('/test',function(){
//     return view('test');
// });

// Untuk komentar dari berita
Route::resource('berita', 'BeritaController');
Route::post('/berita/{beritum}/comment', 'BeritaCommentController@store')->name('berita.comment.store');
